﻿
using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.ServiceInit
{
    public class User : IUser
    {
        private readonly POSContext Context;
        private ApiResponse response;

        public User(POSContext context, ApiResponse response)
        {
            Context = context;
            this.response = response;   
        }

        public async Task<ApiResponse> CreateUser(TblUser use)
        {
            try
            {
                if (!string.IsNullOrEmpty(use.Username) && !string.IsNullOrEmpty(use.Password) && use.EmpId > 0 && use.RoleId > 0)
                {
                    Context.TblUser.Add(use);
                    await Context.SaveChangesAsync();
                    response.Message = "Create User Sucess";
                    response.Code = 200;
                    response.Data = use;
                }
                else
                {
                    response.Message = "Can't create data null";
                    response.Code = 401;
                }
                return response;    
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponse> DeleteUser(int id)
        {
            try
            {
                if (id > 0)
                {
                    var result = await Context.TblUser.Where(x => x.UserId.Equals(id)).SingleOrDefaultAsync();
                    if (result != null)
                    {
                        result.Active = false;
                        await Context.SaveChangesAsync();
                        response.Message = "User was deleted";
                        response.Code = 200;
                        return response;
                    }
                    else
                    {
                        response.Message = "Data Null Can not delete";
                        response.Code = 401;
                        return response;
                    }
                }
                else
                {
                    response.Message = "UserId must greater than zero";
                    response.Code = 401;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }

        public async Task<ApiResponse> GetRoleByList()
        {
            try
            {
                var roles = await Context.TblRole.Where(x =>x.Status == true).ToListAsync();
                response.Message = "Sucess";
                response.Code = 200;
                response.Data = roles;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }

        public async Task<ApiResponse> GetUserById(int id)
        {
            try
            {
                var result = await Context.TblUser.Where(x => x.UserId.Equals(id)).SingleOrDefaultAsync();
                if (result != null)
                {
                    response.Data = result;
                    response.Message = "Get User By ID Success";
                    response.Code = 200;
                    return response;
                }
                else
                {
                    response.Message = "User Get Failed";
                    response.Code = 401;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }

        public async Task<ApiResponse> ListUser()
        {
            try
            {
                //var user = await (from u in Context.TblUser
                //                  join r in Context.TblRole on u.RoleId equals r.RoleId
                //                  join e in Context.TblEmployee on u.EmpId equals e.EmpId
                //                  select new
                //                  { u.UserId, u.EmpId, u.Email, u.RoleId, u.Password, u.Username,  u.DateCreate,u.Active,r.RoleName,e.FullNameEn}).Where(x => x.Active == true).ToListAsync();
                var user = await Context.VwGetUsers.Where(x => x.Active == true).ToListAsync();
                if (user != null)
                {
                    response.Message = "List User Success";
                    response.Code = 200;
                    response.Data = user;
                    return response;
                }
                else
                {
                    response.Message = "Failed";
                    response.Code = 200;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponse> UpdateUser(TblUser user)
        {
            try
            {
                if (user != null && user.UserId > 0 && user.Username != null && user.Password != null && user.EmpId > 0 && user.RoleId > 0)
                {
                    var data = await Context.TblUser.Where(x => x.UserId.Equals(user.UserId)).SingleOrDefaultAsync();
                    if (data != null)
                    {
                        data.Username = user.Username;
                        data.Password = user.Password;
                        data.EmpId = user.EmpId;
                        data.Email = user.Email;
                        data.RoleId = user.RoleId;
                        data.DateCreate = user.DateCreate;

                        await Context.SaveChangesAsync();
                        response.Message = "User Saved Success";
                        response.Code = 200;
                        response.Data = data;
                        return response;
                    }
                    else
                    {
                        response.Message = "No Data, User update failed ";
                        response.Code = 401;
                        return response;
                    }
                }
                else
                {
                    response.Message = "userId null can't update";
                    response.Code = 401;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }
    }
}
