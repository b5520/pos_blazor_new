﻿using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.ServiceInit
{

    public class Employee : IEmployee
    {
        private readonly POSContext Context;
        private ApiResponse response;
        public Employee(POSContext context, ApiResponse response)
        {
            Context = context;
            this.response = response;
        }



        //==============================================
        // Create employee
        //==============================================
        public async Task<ApiResponse> CreateEmployee(TblEmployee emp)
        {
            try
            {
                if (emp.FnameKh != null && emp.LnameKh != null && emp.FullNameEn != null)
                {
                    Context.TblEmployee.Add(emp);
                    await Context.SaveChangesAsync();
                    response.Code = 200;
                    response.Message = "success";
                }
                else
                {
                    response.Code = 401;
                    response.Message = "Bad request";
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }


        //==============================================
        // Delete employee
        //==============================================
        public async Task<ApiResponse> DeleteEmployee(int id)
        {
            try
            {
                if (id > 0)
                {
                    var emp = await Context.TblEmployee.Where(x => x.EmpId.Equals(id)).SingleOrDefaultAsync();
                    if (emp != null)
                    {
                        // you bussiness logic here
                        //Context.Remove(emp);
                        emp.Isactive = false;
                        await Context.SaveChangesAsync();

                        response.Code = 200;
                        response.Message = "success";
                        return response;
                    }
                    else
                    {
                        response.Code = 401;
                        response.Message = "bad request";
                        return response;
                    }
                }
                else
                {
                    response.Code = 401;
                    response.Message = "bad request";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }
        //================================
        //GetEmployeeByID
        //================================
        public async Task<ApiResponse> GetEmployeeById(int id)
        {
            try
            {
                var emp = await Context.TblEmployee.Where(x => x.EmpId.Equals(id)).SingleOrDefaultAsync();
                response.Data = emp;
                response.Code = 200;
                response.Message = "success";
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }



        //==============================================
        // Employee list can use vw or table
        //==============================================

        public async Task<ApiResponse> ListEmployee()
        {
            try
            {
                var emp = await Context.TblEmployee.Where(x => x.Isactive == true).ToListAsync();
                response.Code = 200;
                response.Message = "success";
                response.Data = emp;
                return response;
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }




        //==============================================
        // Update employee
        //==============================================
        public async Task<ApiResponse> UpdateEmployee(TblEmployee employee)
        {
            try
            {
                if (employee != null && employee.EmpId > 0)
                {
                    var emp = Context.TblEmployee.Where(x => x.EmpId.Equals(employee.EmpId)).SingleOrDefault();
                    if (emp != null)
                    {
                        emp.FnameKh = employee.FnameKh;
                        emp.LnameKh = employee.LnameKh;
                        emp.FullNameEn = employee.FullNameEn;
                        emp.Dob = employee.Dob;
                        emp.Gender = employee.Gender;
                        emp.Position = employee.Position;
                        emp.Hiredate = employee.Hiredate;
                        emp.Contact = employee.Contact;
                        emp.Address = employee.Address;
                        emp.Salary = employee.Salary;
                        emp.Isactive = employee.Isactive;

                        await Context.SaveChangesAsync();
                        response.Code = 200;
                        response.Message = "success";
                        response.Data = emp;
                        return response;
                    }
                    else
                    {
                        response.Code = 401;
                        response.Message = "bad request";
                        return response;
                    }
                }
                else
                {
                    response.Code = 401;
                    response.Message = "bad request";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}
