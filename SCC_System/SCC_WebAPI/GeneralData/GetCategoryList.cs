﻿using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.DB.Model;

namespace SCC_WebAPI.GeneralData
{
    public class GetCategoryList
    {
        private readonly POSContext Context;
        private ApiResponse respone;

        public GetCategoryList(POSContext context, ApiResponse respone)
        {
            Context = context;
            this.respone = respone;
        }
        public async Task<ApiResponse> GetCategoryByList()
        {
            try
            {
                var cat = await Context.TblCategory.ToListAsync();
                respone.Code = 200;
                respone.Data = cat;
                return respone; 
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        }
}
