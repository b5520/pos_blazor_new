﻿ using Microsoft.AspNetCore.Components;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.ServicesInit
{
    public class Product : IProduct
    {
        private readonly HttpClient Client;
        public Product(HttpClient httpClient)
        {
            this.Client = httpClient;
        }
        public async Task<ApiResponse> CreateProduct(TblProducts pro)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/Product/CreateProduct", pro);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> DeleteProduct(int id)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Product/DeleteProduct/" + id);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ApiResponse> GetCategoryByList()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Product/GetCategoryByList");
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ApiResponse> GetProductById(int id)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Product/GetProductById/" + id);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> ListProduct()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Product/ListProduct");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> UpdateProduct(TblProducts product)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/Product/UpdateProduct", product);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
