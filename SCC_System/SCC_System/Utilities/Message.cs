﻿using Radzen;

namespace SCC_System.Utilities
{
    public class Message
    {
        public static void Success(NotificationService NotificationService, string Message)
        {
            NotificationService.Notify(new NotificationMessage
            {
                Severity = NotificationSeverity.Info,
                Summary = "បញ្ជាក់",
                Detail = Message,
                Duration = 50000
            });
        }
        public static void Error(NotificationService NotificationService, string Message)
        {
            NotificationService.Notify(new NotificationMessage
            {
                Severity = NotificationSeverity.Error,
                Summary = "បញ្ជាក់",
                Detail = Message,
                Duration = 50000
            });
        }
    }

}
