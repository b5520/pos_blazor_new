using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using SaleModal.Services;
using SCC_System;
using SCC_System.Controllers.Employee;
using SCC_System.Services;
using SCC_System.ServicesInit;
using SCC_System.Utilities;
using Syncfusion.Blazor.Buttons;
using Syncfusion.Blazor;
using Radzen;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

var API_URL = builder.Configuration.GetSection("API_URL").Value;

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(API_URL) });



//===============advertise here new class for UI =============
builder.Services.AddScoped<IEmployee, Employee>();
builder.Services.AddScoped<IProduct, Product>();
builder.Services.AddScoped<ICategory, Category>();
builder.Services.AddScoped<IUser, User>();
builder.Services.AddScoped<ISales, Sales>();
builder.Services.AddScoped<IRolePermission, RolePermission>();

builder.Services.AddScoped<ResponseData>();
builder.Services.AddScoped<ProductResponse>();
builder.Services.AddScoped<NotificationService>();
builder.Services.AddScoped<CategoryResponse>();
builder.Services.AddScoped<UserResponse>();
builder.Services.AddScoped<RoleResponse>();
builder.Services.AddScoped<SaleResponse>();


//builder.Services.AddBootstrapCSS();

await builder.Build().RunAsync();

