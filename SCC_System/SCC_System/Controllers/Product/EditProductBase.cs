﻿
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.JSInterop;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;


namespace SCC_System.Controllers.Product
{
    public class EditProductBase : ComponentBase
    {
        [Parameter]
        public string? productId { get; set; }
        [Inject]
        public ProductResponse? Pro { get; set; }
        public TblProducts? Product { get; set; }
        public List<TblCategory>? Categories { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }
        [Inject]
        private NavigationManager? Navigation { get; set; }
        [Inject]
        NotificationService NotificationService { get; set; }

        public string? imageData;
        public async Task UploadImg(InputFileChangeEventArgs e)
        {

            var formart = "image/png";
            var resizeImage = await e.File.RequestImageFileAsync(formart, 300, 300);
            var buffer = new byte[resizeImage.Size];
            await resizeImage.OpenReadStream().ReadAsync(buffer);
            imageData = $"data:{formart};base64,{Convert.ToBase64String(buffer)}";
            Product.Url = imageData;

        }
        protected override async Task OnInitializedAsync()
        {
            Categories = new List<TblCategory>();
            if (productId != null)
            {
                int id = int.Parse(productId);
                Product = await Pro.GetProductById(id);

                //call from api
                Categories = await Pro.GetCategoryByList();
                StateHasChanged();
            }
        }

        //=======================================
        //edit product
        //============================
        public async Task SaveProduct()
        {
            bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់កែប្រែមែនទេ?");
            if (result)
            {
                if (Product != null && Pro != null)
                {
                    if (!string.IsNullOrEmpty(Product.ProductNameEn) && !string.IsNullOrEmpty(Product.ProductNameKh) && Product.CateId > 0 && Product.Price > 0 && Product.Qty > 0)
                    {
                        await Pro.EditProduct(Product);
                        Message.Success(NotificationService, "ជោគជ័យ");
                        StateHasChanged();
                        Navigation.NavigateTo("/productlist");
                    }
                    else
                    {
                        Message.Error(NotificationService, "មិនមានទិន្នន័យគ្រប់គ្រាន់");
                    }
                }
            }
        }
        public void onBtnExitProd()
        {
            Navigation.NavigateTo("/productlist");
        }
    }

}
