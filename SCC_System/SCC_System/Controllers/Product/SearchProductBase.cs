﻿using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using SaleModal.DB.MyModel;

namespace SCC_System.Controllers.Product
{
    public class SearchProductBase : ComponentBase
    {
        [Inject]
        NavigationManager NavigationManager { get; set; }

      

        public ProductModel modelPro = new ProductModel();

        protected override async Task OnInitializedAsync()
        {
           
        }


        public void onBtnSearch()
        {
            NavigationManager.NavigateTo($"/productlist{JsonConvert.SerializeObject(modelPro)}");
        }

        public void onBtnClear()
        {
            modelPro = new ProductModel();
            StateHasChanged();
        }

        public void onBack()
        {
            NavigationManager.NavigateTo("/productlist");
        }
    }
}
