﻿using BlazorInputFile;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.Extensions.Logging;
using Radzen;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.Product
{
    public class AddProductBase : ComponentBase
    {
        [Inject]
        public ProductResponse? Pro { get; set; }
        public TblProducts? Product { get; set; } 
        public List<TblCategory>? Categories { get; set; }
        [Inject]
        private NavigationManager navigation { get; set; }
        [Inject]
        NotificationService NotificationService { get; set; }

       public  string? imageData;
        public async Task UploadImg(InputFileChangeEventArgs e)
        {
            
            var formart = "image/png";
            var resizeImage = await e.File.RequestImageFileAsync(formart, 300, 300);
            var buffer = new byte[resizeImage.Size];
            await resizeImage.OpenReadStream().ReadAsync(buffer);
            imageData = $"data:{formart};base64,{Convert.ToBase64String(buffer)}";
            Product.Url = imageData;

        }

        protected override async Task OnInitializedAsync()
        {
            Categories = new List<TblCategory>();
            Product = new TblProducts();
            Categories = await Pro.GetCategoryByList();
        }

        public async Task AddNewProduct()
        {
            if (Product != null && Pro != null)
            {
                if (!string.IsNullOrEmpty(Product.ProductNameEn) && !string.IsNullOrEmpty(Product.ProductNameKh) && 
                    Product.CateId != 0 && Product.Price > 0 &&  Product.Price > 0 && Product.Url != null)              
                {
                    Product.Status = true;
                    await Pro.AddNewProduct(Product);
                    Message.Success(NotificationService, "ជោគជ័យ");
                    await Pro.GetCategoryByList();
                    StateHasChanged();
                    navigation.NavigateTo("productlist");
                }      

            }
        }
        public void onBtnExitNewProd()
        {
            navigation.NavigateTo("/productlist");
        }
    }
}