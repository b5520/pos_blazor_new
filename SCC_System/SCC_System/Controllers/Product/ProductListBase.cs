﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SaleModal.DB.Model;
using SCC_System.Services;

namespace SCC_System.Controllers.Product
{
    public class ProductListBase : ComponentBase
    {
        [Inject]
        NavigationManager Navigate { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }
        [Inject]
        public ProductResponse? Pro { get; set; }
        public List<VwSelectAllProduct>? ProductList { get; set; }

        protected async override Task OnInitializedAsync()
        {
            ProductList = await Pro.ProductsList();
        }
        public void onBtnAddNew()
        {
            Navigate.NavigateTo("/product/info");
        }
        public void onBtnNewCategory()
        {
            Navigate.NavigateTo("/categorylist");
        }

        public async void onBtnRefresh()
        {
            ProductList = await Pro.ProductsList();
        }

        public async Task DeleteProduct(int id)
        {
            bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់លុបមែនទេ?");
            if (result)
            {
                await Pro.DeleteProduct(id);
                ProductList = await Pro.ProductsList();
                StateHasChanged();
            }
           
        }

    }
}
