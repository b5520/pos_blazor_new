﻿using Microsoft.AspNetCore.Components;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.RolePermission
{
    public class AddRolePermissionBase : ComponentBase
    {
        [Inject]
        NotificationService NotificationService { get; set; }
        [Inject]
        private NavigationManager navigation { get; set; }
        [Inject]
        public RoleResponse? rolerespon { get; set; }
        [Inject]
        public UserResponse? userrespon { get; set; }
        public TblRolePermission? rolepermission { get; set; }
        public List<TblRole> roleList = new List<TblRole>();
        public List<TblPermission> permissionList = new List<TblPermission>();

        protected override async Task OnInitializedAsync()
        {
            rolepermission = new TblRolePermission();
            this.roleList = await this.userrespon.GetRoleByList();
            this.permissionList = await this.rolerespon.GetPermissionList();
        }

        public async Task OnNewRolePermission()
        {
            if (rolepermission != null && rolerespon != null && userrespon != null)
            {
                if (rolepermission.RoleId > 0 && rolepermission.PerId > 0)
                {
                    rolepermission.Status = true;
                    await rolerespon.AddRolePermission(rolepermission);
                    Message.Success(NotificationService, "ជោគជ័យ");
                    await userrespon.GetRoleByList();
                    await rolerespon.GetPermissionList();
                    StateHasChanged();
                    navigation.NavigateTo("/userRole");
                }
            }
        }
        public void onBtnExitUserRole()
        {
            navigation.NavigateTo("/userRole");
        }

    }
}
