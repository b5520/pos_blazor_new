﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SaleModal.DB.Model;
using SCC_System.Services;

namespace SCC_System.Controllers.RolePermission
{
    public class ListRolePermissionBase : ComponentBase
    {
        [Inject]
        public IJSRuntime Runtime { get; set; }
        [Inject]
        private NavigationManager navigation { get; set; }
        [Inject]
        public UserResponse? userResponse { get; set; }
        [Inject]
        public RoleResponse? rolerespon { get; set; }
        public List<TblRole>? rolelist { get; set; }
        public List<VwGetRolePermission>? rolepermissions { get; set; }
        public List<TblPermission>? permissionlist { get; set; }

        protected override async Task OnInitializedAsync()
        {
            rolelist = await userResponse.GetRoleByList();
            rolepermissions = await rolerespon.ListRolePermission();
            permissionlist = await rolerespon.GetPermissionList();
        }

        public void onBtnNewRole()
        {
            navigation.NavigateTo("/adduserRole");
        }
        public async void onBtnRefresh()
        {
            rolepermissions = await rolerespon.ListRolePermission();
        }
        public async Task OnDeleteRole(int id)
        {
            bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់លុបមែនទេ?");
            if (result)
            {
                await rolerespon.DeleteRolePermission(id);
                rolepermissions = await rolerespon.ListRolePermission();
                StateHasChanged();
            }

        }
    }
}
