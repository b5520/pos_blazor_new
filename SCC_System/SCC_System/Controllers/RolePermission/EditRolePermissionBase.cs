﻿using Microsoft.AspNetCore.Components;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.RolePermission
{
    public class EditRolePermissionBase : ComponentBase
    {
        [Parameter]
        public string? roleperId { get; set; }
        [Inject]
        NotificationService NotificationService { get; set; }
        [Inject]
        private NavigationManager navigation { get; set; }
        [Inject]
        public RoleResponse? rolerespon { get; set; }
        [Inject]
        public UserResponse? userrespon { get; set; }
        public TblRolePermission? rolepermission { get; set; }
        public List<TblRole>? roleList { get; set; }
        public List<TblPermission>? permissionList { get; set; }

        protected override async Task OnInitializedAsync()
        {
            roleList = new List<TblRole>();
            permissionList = new List<TblPermission>();
            if (roleperId != null)
            {
                int id = int.Parse(roleperId);
                rolepermission = await rolerespon.GetRolePerById(id);
                roleList = await userrespon.GetRoleByList();
                permissionList = await rolerespon.GetPermissionList();
                StateHasChanged();
            }
        }

        public async Task OnEditUserRole()
        {
            if (rolepermission != null && rolerespon != null && userrespon != null)
            {
                if (rolepermission.RoleId > 0 && rolepermission.PerId > 0)
                {
                    await rolerespon.EditRolePermission(rolepermission);
                    Message.Success(NotificationService, "ជោគជ័យ");
                    StateHasChanged();
                    navigation.NavigateTo("/userRole");
                }
            }
        }

        public void onBtnExitUserRole()
        {
            navigation.NavigateTo("/userRole");
        }
    }
}
