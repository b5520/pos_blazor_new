﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.User
{
    public class EditUserBase : ComponentBase
    {
        [Parameter]
        public string? userId { get; set; }
        [Inject]
        public UserResponse? ur { get; set; }   
        public TblUser? User { get; set; }
        public List<TblRole>? Roleslists { get; set; }

        [Inject]
        private IJSRuntime? Runtime { get; set; }
        [Inject]
        NotificationService NotificationService { get; set; }
        [Inject]
        NavigationManager Navigation { get;set; }

        protected override async Task OnInitializedAsync()
        {
            Roleslists = new List<TblRole>();
            if (userId != null)
            {
                int id = int.Parse(userId);
                User = await ur.GetUserById(id);
                Roleslists = await ur.GetRoleByList();
                StateHasChanged();
            }
        }

        public async Task SaveUser()
        {
            bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់កែប្រែមែនទេ?");
            if (result)
            {
                if (User != null && ur != null)
                {
                    if(!string.IsNullOrEmpty(User.Username) && !string.IsNullOrEmpty(User.Password) && !string.IsNullOrEmpty(User.Email) && User.EmpId > 0 && User.RoleId > 0)
                    {
                        await ur.EditUser(User);
                        Message.Success(NotificationService, "ជោគជ័យ");
                        StateHasChanged();
                        Navigation.NavigateTo("/account");
                    }
                    else
                    {
                        Message.Error(NotificationService, "មិនមានទិន្នន័យគ្រប់គ្រាន់");
                    }
                }
            }
        }

        public void onBtnExitUser()
        {
            Navigation.NavigateTo("/account");
        }
    }
}
