﻿using Microsoft.AspNetCore.Components;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.User
{
    public class AddUserBase : ComponentBase
    {
        [Inject]
        NotificationService NotificationService { get; set; }
        [Inject]
        private NavigationManager navigation { get; set; }

        [Inject]
        public UserResponse? UR {get;set;}
        public TblUser? User { get; set; }
        public List<TblRole> Roleslists { get; set; }

        protected override async Task OnInitializedAsync()
        {
            Roleslists = new List<TblRole>();
            User = new TblUser();
            Roleslists = await UR.GetRoleByList();
        }

        public async Task AddNewUser()
        {
            if (User != null && UR != null)
            {
                if (!string.IsNullOrEmpty(User.Username)&& !string.IsNullOrEmpty(User.Password) && !string.IsNullOrEmpty(User.Email) && User.EmpId > 0 && User.RoleId > 0)
                {
                    User.Active = true;
                    await UR.AddnewUser(User);
                    Message.Success(NotificationService, "ជោគជ័យ");
                    await UR.GetRoleByList();
                    StateHasChanged();
                    navigation.NavigateTo("/account");
                }
            }
        }

        public void onBtnExitUser()
        {
            navigation.NavigateTo("/account");
        }
    }
}
