﻿
using Microsoft.AspNetCore.Components;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.Category
{
    public class AddCategoryBase : ComponentBase
    {
        [Inject]
        public CategoryResponse? CategoryResponse { get; set; }
        public TblCategory? NewCategory { get; set; }

        [Inject]
        NavigationManager Navigation { get; set; }
        [Inject]
        NotificationService NotificationService  { get; set; }

        protected async override Task OnInitializedAsync()
        {
            NewCategory = new TblCategory();
        }

        public async Task AddNewCategory()
        {
            if (NewCategory != null && CategoryResponse != null)
            {
                if (!string.IsNullOrEmpty(NewCategory.CategoryNameEn) && !string.IsNullOrEmpty(NewCategory.CategoryNameKh))
                {
                    NewCategory.Status = true;
                    await CategoryResponse.CreateNewCategory(NewCategory);
                    Message.Success(NotificationService, "ជោគជ័យ");
                    StateHasChanged();
                    Navigation.NavigateTo("/categorylist");
                }
            }
        }
        public void onBtnExitCategory()
        {
            Navigation.NavigateTo("/categorylist");
        }
    }
}
