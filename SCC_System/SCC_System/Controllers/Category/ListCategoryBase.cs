﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;

namespace SCC_System.Controllers.Category
{
    public class ListCategoryBase : ComponentBase
    {
        [Inject]
        public CategoryResponse? CategoryResponse { get; set; }
        public List<TblCategory>? CategoryList { get; set; }
        [Inject]
        NavigationManager navigation { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }


        protected async override Task OnInitializedAsync()
        {
            CategoryList = await CategoryResponse.CategoryLists();
        }
     
        public async void onBtnRefresh()
        {
            CategoryList = await CategoryResponse.CategoryLists();
        }
        public void onBtnNewCategory()
        {
            navigation.NavigateTo("/newcategory");
        }
        public async Task DeleteCategory(int id)
        {
            bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់លុបមែនទេ?");
            if (result)
            {
                await CategoryResponse.DeleteCategory(id);
                CategoryList = await CategoryResponse.CategoryLists();
                StateHasChanged();
            }
                
        }
    }
}
