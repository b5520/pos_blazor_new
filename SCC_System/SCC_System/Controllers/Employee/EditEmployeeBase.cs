﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Pages.Emloyees;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.Employee
{
    public class EditEmployeeBase : ComponentBase
    {
        [Parameter]
        public string? employeeId { get; set; }
        [Inject]
        public ResponseData? Emp { get; set; }
        public TblEmployee? Employee { get; set; }
        [Inject]
        NavigationManager navigation { get; set; }
        [Inject]
        NotificationService notificationService { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }


        protected override async Task OnInitializedAsync()
        {
            if (employeeId != null)
            {
                int Id = int.Parse(employeeId);
                Employee = await Emp.GetEmployeeById(Id);
                //StateHasChanged();
            }
        }

        //=============================================
        //Event call back for button update employee
        //=============================================
        public async Task SaveEmployee()
        {
            bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់កែប្រែមែនទេ?");
            if (result)
            {
                if (Employee != null && Emp != null)
                {
                    if (!string.IsNullOrEmpty(Employee.FnameKh) && !string.IsNullOrEmpty(Employee.LnameKh) && !string.IsNullOrEmpty(Employee.FullNameEn))
                    {
                        await Emp.SaveEmployee(Employee);
                        Message.Success(notificationService, "ជោគជ័យ");
                        navigation.NavigateTo("/employee");
                        StateHasChanged();
                    }
                }
            }

        }

        public void onBtnExit()
        {
            navigation.NavigateTo("/employee");
        }
    }

}
