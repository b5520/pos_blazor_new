﻿using Microsoft.AspNetCore.Components;
using SaleModal.DB.Model;
using SCC_System.Pages.Emloyees;
using SCC_System.Services;
using SaleModal.Classes;


namespace SCC_System.Controllers.Employee
{
    public class AddEmployeeBase : ComponentBase
    {

        [Inject]
        public ResponseData? Emp { get; set; }
        public TblEmployee? Employee { get; set; }
        [Inject]
        NavigationManager navigation { get; set; }  

        protected override async Task OnInitializedAsync()
        {
            Employee = new TblEmployee();
        }

        public async Task CreateEmployee()
        {
                if (Employee != null && Emp != null)
                {
                    if (!string.IsNullOrEmpty(Employee.FnameKh) && !string.IsNullOrEmpty(Employee.LnameKh) && !string.IsNullOrEmpty(Employee.FullNameEn) &&
                        !string.IsNullOrEmpty(Employee.Gender) && !string.IsNullOrEmpty(Employee.Position) && Employee.Dob < DateTime.Now)
                    {
                        Employee.Isactive = true;
                        await Emp.CreateEmployee(Employee);
                        navigation.NavigateTo("/employee");
                    }
                } 
         }
        public void onBtnExitAddEmp()
        {
            navigation.NavigateTo("/employee");
        }

    }
       
}

