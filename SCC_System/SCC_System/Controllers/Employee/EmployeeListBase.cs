﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SaleModal.DB.Model;
using SCC_System.Services; 

namespace SCC_System.Controllers.Employee
{
    public class EmployeeListBase : ComponentBase
    {
        [Inject]
        NavigationManager Navigate { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }

        [Inject]
        public ResponseData? Emp { get; set; }
        public List<TblEmployee>? EmployeeList { get; set; }

        protected async override Task OnInitializedAsync()
        {
             EmployeeList = await Emp.EmployeesList();
        }
        public void onBtnAddNew()
        {
            Navigate.NavigateTo("/employee/info");
        }
       
        public async void onBtnRefresh()
        {
            EmployeeList = await Emp.EmployeesList();

        }
    
        public async Task DeleteEmployee(int id)
        {
            bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់លុបមែនទេ?");
            if (result)
            {
                await Emp.DeleteEmployee(id);
                EmployeeList = await Emp.EmployeesList();
                StateHasChanged();
            }
        }

    }
}


