﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SCC_System.Services;

namespace SCC_System.Controllers.Sales
{
    public class SaleBase : ComponentBase
    {
        [Inject]
        private IJSRuntime? Runtime { get; set; }
        [Inject]
        public SaleResponse Sale { get; set; }
        [Inject]
        public CategoryResponse Category { get; set; }
        public List<VwSelectAllProduct> ListProduct = new List<VwSelectAllProduct>();
        public List<TblOrderDetail> OrderDetails = new List<TblOrderDetail>();
        public List<TblCategory> Categories = new List<TblCategory>();
        public float GrandTotal { get; set; }
        public float CashRecieve { get; set; }
        public float PayBack { get; set; }
        public SalePost salePost = new SalePost();
        public float Total { get; set; }
        public float Discount { get; set; }
        public bool isStateChange { get; set; } = false;

        protected override async Task OnInitializedAsync()
        {
            this.ListProduct = await this.Sale.GetProducts();
            this.Categories = await this.Category.CategoryLists();
            StateHasChanged();

        }

        public async void OnClickCate(int CateId)
        {
            var list = await this.Sale.GetProducts();
            ListProduct = list.Where(f => f.CateId == CateId).ToList();
            StateHasChanged();
        }

        public async Task OnSaveOrder()
        {
            if (salePost != null)
            {
                if (salePost.details == null)
                {
                    await Runtime.InvokeAsync<object>("warning", "សូមបញ្ចូលទំនិញមុនធ្វើការទូរទាត់!");
                }
                else if (salePost.CashRevieve == 0)
                {
                    await Runtime.InvokeAsync<object>("warning", "សូមបញ្ចូលប្រាក់ទទួល!");
                }
                else if (salePost.CashRevieve < GrandTotal)
                {
                    await Runtime.InvokeAsync<object>("warning", "ទឹកប្រាក់មិនអាចតូចជាងតម្លៃសរុប​!");
                }
                else
                {
                    var result = await Sale.AddSale(salePost);
                    if (result.Code == 200)
                    {
                        GrandTotal = 0;
                        salePost = new SalePost();
                        OrderDetails = new List<TblOrderDetail>();
                        Total = 0;
                        CashRecieve = 0;
                        PayBack = 0;
                        StateHasChanged();
                    }
                    else
                    {
                        await Runtime.InvokeAsync<object>("warning", result.Message);
                    }

                }
            }
        }
        public async Task OnRemove(TblOrderDetail orderDetail)
        {
            if (orderDetail.Qty > 1)
            {
                orderDetail.Qty = orderDetail.Qty - 1;
                OrderDetails = OrderDetails.Where(f => f.ProId != orderDetail.ProId).ToList();          //remove item from orderDetail
                OrderDetails.Add(orderDetail);
                OrderDetails = OrderDetails.ToList();
                StateHasChanged();
            }
            else
            {
                OrderDetails.Remove(orderDetail);
                OrderDetails = OrderDetails.ToList();
                StateHasChanged();
            }
        }

        public async Task OnCancelOrder()
        {
            GrandTotal = 0;
            salePost = new SalePost();
            OrderDetails = new List<TblOrderDetail>();
            Total = 0;
            CashRecieve = 0;
            PayBack = 0;
            StateHasChanged();
        }

        public async Task ProductClick(VwSelectAllProduct pro)
        {
            GrandTotal = 0;
            isStateChange = false;
            TblOrderDetail detail = new TblOrderDetail();
            detail.ProId = pro.ProId;
            detail.ProNameKh = pro.ProductNameKh;
            detail.ProNameEn = pro.ProductNameEn;
            detail.Price = Convert.ToDouble(pro.Price);
            detail.Qty = 1;

            var result = OrderDetails.Where(x => x.ProId == pro.ProId).ToList();
            if (result.Count() > 0)
            {
                result.FirstOrDefault().Qty++;
            }
            else
            {
                OrderDetails.Add(detail);
                OrderDetails = OrderDetails.ToList();
            }
            salePost.GrandTotal = GrandTotal;
            salePost.CashRevieve = CashRecieve;
            salePost.PayBack = PayBack;
            salePost.OrderDate = DateTime.Now.Date;
            salePost.details = OrderDetails;
            StateHasChanged();
        }

        public async Task CashRecieveChange(ChangeEventArgs e)
        {
            if (e.Value != null && !string.IsNullOrEmpty(e.Value.ToString()))
            {
                float cash = float.Parse(e.Value.ToString());
                CashRecieve = cash;
                if (CashRecieve > GrandTotal)
                {
                    PayBack = CashRecieve - GrandTotal;
                }
                else
                {
                    PayBack = 0;
                }
            }
            else
            {
                PayBack = 0;
            }
            isStateChange = true;
            StateHasChanged();


        }

    }
}
