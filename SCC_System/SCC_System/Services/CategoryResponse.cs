﻿using Newtonsoft.Json;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.Services
{
    public class CategoryResponse
    {
        private readonly ICategory Cate;
        public List<TblCategory> CategoryList { get; set; }
        public TblCategory Category { get; set; }
        ApiResponse response;

        public CategoryResponse(ICategory cate)
        {
            Cate = cate;
        }
        public async Task<List<TblCategory>> CategoryLists()
        {
            CategoryList = new List<TblCategory>();
            try
            {
                response = await Cate.ListCategory();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        CategoryList = JsonConvert.DeserializeObject<List<TblCategory>>(Data);
                    }
                }
                return CategoryList;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async Task<TblCategory>GetCategoryById(int id)
        {
            try
            {
                response = await this.Cate.GetCategoryById(id);
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        Category = JsonConvert.DeserializeObject<TblCategory>(Data);
                    }
                }
                return Category;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async Task EditCategory(TblCategory category)
        {
            try
            {
                response = await this.Cate.UpdateCategory(category);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async Task DeleteCategory(int id)
        {
            try
            {
                response = await this.Cate.DeleteCategory(id);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async Task CreateNewCategory(TblCategory category)
        {
            try
            {
                response = await this.Cate.CreateCategory(category);
            }
            catch (Exception ex)
            {

                throw;
            }   
        }
     }
}
