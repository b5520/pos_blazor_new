﻿using Newtonsoft.Json;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.Services
{
    public class SaleResponse
    {
        private readonly ISales sale;
        ApiResponse response;
        public List<VwSelectAllProduct> ProductList { get; set; }
        public SaleResponse(ISales sales)
        {
            this.sale = sales;
        }

        public async Task<List<VwSelectAllProduct>> GetProducts()
        {
            try
            {
                response = await this.sale.GetListProduct();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        ProductList = JsonConvert.DeserializeObject<List<VwSelectAllProduct>>(Data);
                    }
                }
                return ProductList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task AddSale(SalePost post)
        {
            try
            {
                response = await this.sale.CreateSale(post);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


    }
}
