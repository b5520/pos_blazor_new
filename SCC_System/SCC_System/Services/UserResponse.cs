﻿using Newtonsoft.Json;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.Services
{
    public class UserResponse 
    {
        private readonly IUser User;
        public TblUser? tbUser { get; set; }
        public List <VwGetUsers> ListUser { get; set; }
        public List<TblRole> RoleList { get; set; }
        ApiResponse response;

        public UserResponse(IUser user)
        {
            User = user;    
        }

        public async Task<List<VwGetUsers>> UserLists()
        {
            ListUser = new List<VwGetUsers>();
            try
            {
                response = await User.ListUser();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        ListUser = JsonConvert.DeserializeObject<List<VwGetUsers>>(Data);
                    }
                }
                return ListUser;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async Task<List<TblRole>> GetRoleByList()
        {
            RoleList = new List<TblRole>();
            try
            {
                response = await this.User.GetRoleByList();
                if (response.Code == 200 & response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        RoleList = JsonConvert.DeserializeObject<List<TblRole>>(Data);
                    }
                }
                return RoleList;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<TblUser>GetUserById(int id)
        {
            try
            {
                response = await this.User.GetUserById(id);
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        tbUser = JsonConvert.DeserializeObject<TblUser>(Data);
                    }
                }
                return tbUser;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task AddnewUser(TblUser user)
        {
            try
            {
                response = await this.User.CreateUser(user);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task DeleteUser(int id)
        {
            try
            {
                response = await this.User.DeleteUser(id);  
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task EditUser(TblUser user)
        {
            try
            {
                response = await this.User.UpdateUser(user);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
