﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SaleModal.Classes;
using SaleModal.DB.Model;

namespace SaleModal.Services
{
    public interface IProduct
    {
        public Task<ApiResponse> ListProduct();
        public Task<ApiResponse> CreateProduct(TblProducts pro);
        public Task<ApiResponse> UpdateProduct(TblProducts product); 
        public Task<ApiResponse> DeleteProduct(int id);
        public Task<ApiResponse> GetProductById(int id);
        public Task<ApiResponse>GetCategoryByList();
    }
}
