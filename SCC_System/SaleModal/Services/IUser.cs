﻿using SaleModal.Classes;
using SaleModal.DB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaleModal.Services
{
    public interface  IUser
    {
        public Task<ApiResponse> ListUser();
        public Task<ApiResponse> CreateUser(TblUser use);
        public Task<ApiResponse> UpdateUser(TblUser user);
        public Task<ApiResponse> GetUserById(int id);
        public Task<ApiResponse> DeleteUser(int id);
        public Task<ApiResponse> GetRoleByList();
    }
}
