﻿using SaleModal.Classes;
using SaleModal.DB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaleModal.Services
{

    public  interface ISales
    {
        public Task<ApiResponse> ListSales();
        public Task<ApiResponse> CreateSale(SalePost post);
        public Task<ApiResponse> GetSaleDetail(int saleId);
        public Task<ApiResponse> GetListProduct();
    }
}
