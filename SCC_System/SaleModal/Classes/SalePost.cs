﻿using SaleModal.DB.Model;

namespace SaleModal.Classes
{
    public class SalePost
    {
        public int? UserId { get; set; }
        public float? GrandTotal { get; set; }
        public float? CashRevieve { get; set; }
        public float? PayBack { get; set; }
        public DateTime? OrderDate { get; set; }
        public List<TblOrderDetail> details { get; set; }
    }
}
