﻿using SaleModal.DB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaleModal.Classes
{
    public class SaleDetail
    {
        public int? ProId { get; set; }
        public string ProNameEn { get; set; }
        public string ProNameKh { get; set; }
        public int? Qty { get; set; }
        public double? Price { get; set; }
        public double? Total { get; set; }
    }
}
