﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaleModal.Classes
{
    public class ApiResponse
    {
        public string? Message { get; set; }
        public int Code { get; set; }
        public Object? Data { get; set; }

    }
}
